window.addEventListener("load", (ev: Event) => {
	const element = document.getElementById("app");
	if (element) {
		const racetrack = new SomeModule.RaceTrack(element);
		racetrack.showTime();
	}
});
