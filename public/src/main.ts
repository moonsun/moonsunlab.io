module SomeModule {
	export class RaceTrack {
		constructor(private host: Element) {
			console.info({ host });
		}

		showTime(): void {
			const p = document.createElement("p");
			p.className = "subtle";
			const newDate = new Date();
			p.innerText = newDate.toString();
			this.host.appendChild(p);
		}
	}
}
