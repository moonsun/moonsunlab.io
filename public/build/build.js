window.addEventListener("load", (ev) => {
    const element = document.getElementById("app");
    if (element) {
        const racetrack = new SomeModule.RaceTrack(element);
        racetrack.showTime();
    }
});
var SomeModule;
(function (SomeModule) {
    class RaceTrack {
        host;
        constructor(host) {
            this.host = host;
            host.appendChild(document.createElement("canvas"));
        }
        showTime() {
            const p = document.createElement("p");
            p.className = "subtle";
            const newDate = new Date();
            p.innerText = newDate.toString();
            this.host.appendChild(p);
        }
    }
    SomeModule.RaceTrack = RaceTrack;
})(SomeModule || (SomeModule = {}));
//# sourceMappingURL=build.js.map